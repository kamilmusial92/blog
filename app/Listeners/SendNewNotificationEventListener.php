<?php

namespace App\Listeners;

use App\Events\BlogPostCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notification;

class SendNewNotificationEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPostCreatedEvent  $event
     * @return void
     */
    public function handle(BlogPostCreatedEvent $event)
    {
             Notification::create([
            
            'content'=>'Został utworzony nowy post o tytule '.$event->blogpost->title.' przez '.$event->user->name.' .'
        ]);

    }
}
