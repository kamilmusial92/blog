<?php

namespace App\Listeners;

use App\Events\SendEmailRegisteredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\UserRegistered;

class SendEmailRegisteredEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmailRegisteredEvent  $event
     * @return void
     */
    public function handle(SendEmailRegisteredEvent $event)
    {
         \Mail::to($event->user->email)->send(
            new UserRegistered($event->user)
        );
    }
}
