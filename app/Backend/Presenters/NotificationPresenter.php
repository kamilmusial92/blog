<?php
namespace App\Backend\Presenters;


trait NotificationPresenter {	
	
	public function getCreatedAttribute()
	{
		$str=date('H:i d-m-Y',strtotime($this->created_at));
		
		return $str;

	}




}