<?php
namespace App\Backend\Repositories;

use Cache;

use App\Backend\Interfaces\FrontendRepositoryInterface;


class CachedFrontendRepository  extends FrontendRepository implements FrontendRepositoryInterface{

	
    public function getPosts()
    {

       return Cache::remember('getPosts',1140,function() {

            return parent::getPosts();

        });


    }

    public function getPost($id)
    {
         return Cache::remember('getPost'.$id,1140,function() use ($id){

            return parent::getPost($id);

        });
    }


}
