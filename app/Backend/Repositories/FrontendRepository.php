<?php
namespace App\Backend\Repositories;
use App\{BlogPost};
use App\Backend\Interfaces\FrontendRepositoryInterface;



class FrontendRepository implements FrontendRepositoryInterface{

	public function getPosts()
 	{
 		return BlogPost::orderBy('created_at','DESC')->paginate(10);
 	}

 	public function getPost($id)
 	{
 		return BlogPost::find($id);
 	}

}