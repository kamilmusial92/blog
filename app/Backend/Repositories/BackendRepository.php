<?php
namespace App\Backend\Repositories;
use App\{BlogPost,User,Role,Notification};
use App\Backend\Interfaces\BackendRepositoryInterface;
use Auth;
use Illuminate\Support\Facades\Hash;

use File;

class BackendRepository implements BackendRepositoryInterface{

 	public function getPosts()
 	{
 		return BlogPost::orderBy('created_at','DESC')->paginate(10);
 	}

 	public function getNotifications()
 	{
 		return Notification::orderBy('created_at','DESC')->paginate(10);
 	}

 	public function getPost($id)
 	{
 		return BlogPost::find($id);
 	}

 	public function UploadImage($request,$id)
 	{
 		 $file =$request->file('image');
         $filename =  preg_replace('/[^a-zA-Z0-9-_\.]/','',$file->getClientOriginalName());

         $file->move(('storage/images'), $filename);
       		
       	$post=BlogPost::find($id)->update(['file_path'=>$filename]);
       
        
 	}

 	public function DeleteImage($id)
 	{
 		$post=BlogPost::find($id);

 		$filename='storage/images/'.$post->file_path;
           if (file_exists($filename)) {
            unlink($filename);
       }
 	}

 	public function NewPost($request)
 	{
 		$blog=BlogPost::create(['title'=>$request->title,'content'=>$request->content,'user_id'=>Auth::user()->id]);
 		if($request->image)
 		$this->UploadImage($request,$blog->id);
 	return $blog;
 	}

 	public function SavePost($request,$id)
 	{
 		$post=BlogPost::find($id);

 		$post->update(['title'=>$request->title,'content'=>$request->content,'user_id'=>Auth::user()->id]);
 		if($request->image)
 		$this->DeleteImage($post->id);
 		$this->UploadImage($request,$post->id);
 	}

 	public function DeletePost($id)
 	{
 		$post=BlogPost::find($id);
 		if($post->image)
 		{
 			$this->DeleteImage($post->id);
 		}

 		$post->delete();

 	}

 	public function getUsers()
 	{
 		return User::with(['roles'])->paginate(10);
 	}

 		public function getUser($id)
 	{
 		return User::with(['roles'])->find($id);
 	}

 	public function UpdateRoleUser($data,$user)
 	{
 		$searchrole=Role::find($data->role);

 		
 		
 		$user->roles()->detach();

 		if($searchrole)
 		{


 			$user->roles()->attach($searchrole->id);
 		}
 		else{
 			$user->roles()->attach(1);
 		}
 	}

 	public function NewUser($request)
 	{
 		$user=User::create(['name'=>$request->name,'email'=>$request->email,'password'=>Hash::make($request->password)]);
 		
 		$updaterole=$this->UpdateRoleUser($request,$user);
 		return $user;
 	}

 		public function SaveUser($request,$id)
 	{
 		$user=User::find($id);

 		$user->update(['email'=>$request->email,'name'=>$request->name]);

 		$updaterole=$this->UpdateRoleUser($request,$user);
 	}

 	public function DeleteUser($id)
 	{
 		$user=User::find($id);
 		$user->delete();

 	}

}
