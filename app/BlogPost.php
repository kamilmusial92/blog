<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    use Backend\Presenters\PostPresenter;

     protected $fillable = [
        'title', 'content','user_id','file_path'
    ];
}
