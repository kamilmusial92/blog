<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	 use Backend\Presenters\NotificationPresenter;

     protected $fillable = [
      'content'
    ];
}
