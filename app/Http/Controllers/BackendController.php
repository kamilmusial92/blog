<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Backend\Interfaces\BackendRepositoryInterface;

use App\Http\Requests\CreatePost;
use App\Http\Requests\CreateUser;
use App\Http\Requests\UpdateUser;
use Session;
use Auth;
use Cache;

use App\Events\BlogPostCreatedEvent;
use App\Events\SendEmailRegisteredEvent;

class BackendController extends Controller
{
    public function __construct(BackendRepositoryInterface $bR)
    {

        $this->middleware('CheckUser');
        $this->middleware('CheckAdmin')->only(['users','createuser','newuser','edituser','saveuser','deleteuser']);
        $this->bR=$bR;
        
    }

    public function index(){

    	$posts=$this->bR->getPosts();

    	return view('backend.index',['posts'=>$posts]);
    }

    public function notifications(){

        $notifications=$this->bR->getNotifications();

        return view('backend/notifications.index',['notifications'=>$notifications]);
    }

    public function createpost()
    {
    	return view('backend/posts.create');
    }

    public function newpost(CreatePost $request)
    {
    	$new=$this->bR->NewPost($request);
    	Session::flash('success','Utworzono nowy post.');

        event(new BlogPostCreatedEvent($new,Auth::user()));

        Cache::flush();

    	return redirect()->route('adminHome');
    }

     public function savepost(CreatePost $request,$id)
    {
    	$this->bR->SavePost($request,$id);
    	Session::flash('success','Edytowano post.');
        Cache::flush();
    	return redirect()->route('adminHome');
    }

     public function editpost($id)
    {
    	$post=$this->bR->getPost($id);
    	return view('backend/posts.edit',['post'=>$post]);
    }

    public function deletepost($id)
    {
    	$this->bR->DeletePost($id);

    	Session::flash('success','Usunięto wybrany post');

    	return redirect()->route('adminHome');
    }

    public function users()
    {

    	$users=$this->bR->getUsers();

    	return view('backend/users.index',['users'=>$users]);
    }

     public function createuser()
    {
    	return view('backend/users.create');
    }

     public function newuser(CreateUser $request)
    {
    	$user=$this->bR->NewUser($request);
    	Session::flash('success','Utworzono nowego użytkownika.');

        event(new SendEmailRegisteredEvent($user));

    	return redirect()->route('admin_users');
    }

       public function edituser($id)
    {
    	$user=$this->bR->getUser($id);
    	return view('backend/users.edit',['user'=>$user]);
    }

       public function saveuser(UpdateUser $request,$id)
    {
    	$this->bR->SaveUser($request,$id);
    	Session::flash('success','Edytowano użytkownika.');
    	return redirect()->route('admin_users');
    }

       public function deleteuser($id)
    {
        $this->bR->DeleteUser($id);

        Session::flash('success','Usunięto użytkownika');

        return redirect()->route('admin_users');
    }
}
