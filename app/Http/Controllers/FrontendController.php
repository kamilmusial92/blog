<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Backend\Interfaces\FrontendRepositoryInterface;


class FrontendController extends Controller
{
      public function __construct(FrontendRepositoryInterface $bR)
    {
    	$this->bR=$bR;
      
       
    }

    public function index()
    {

    	$posts=$this->bR->getPosts();

    	return view('frontend.index',['posts'=>$posts]);
    }

    public function post($id)
    {

    	$post=$this->bR->getPost($id);

    	return view('frontend.post',['post'=>$post]);
    }
}
