<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
           $this->app->bind(
            \App\Backend\Interfaces\BackendRepositoryInterface::class,function(){
            return new \App\Backend\Repositories\BackendRepository;
        });

            if(App::environment('local'))
        {
            $this->app->bind(
                \App\Backend\Interfaces\FrontendRepositoryInterface::class,function(){
                return new \App\Backend\Repositories\FrontendRepository;
            });
        }
        else{
            $this->app->bind(
                \App\Backend\Interfaces\FrontendRepositoryInterface::class,function(){
                return new \App\Backend\Repositories\CachedFrontendRepository;
            });
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
