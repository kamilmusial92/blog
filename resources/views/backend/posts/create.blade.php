@extends('layouts.backend')

@section('content')

<h5 class="card-title">Nowy post</h5>


<form action="{{route('newpost')}}" method='POST' enctype="multipart/form-data">
	@csrf
	<div class="form-group">
		<label>Tytuł</label>
		<input type='text' name="title" value="{{old('title')}}">
	</div>
	 <div class="form-group">
		    <input name="image" type="file"  />
	</div>
	 <div class="form-group">
		<label>Tekst</label>
		<textarea rows="7" class="form-control" name='content'>{{old('content')}}</textarea>
	</div>

 <div class="card-footer">
      <button type="submit" class="btn btn-success">Stwórz</button>
    </div>

</form>



@stop