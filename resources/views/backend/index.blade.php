@extends('layouts.backend')

@section('content')

<h2>Zarządzanie postami</h2>

<a  class="btn btn-success" href="{{route('createpost')}}">Stwórz post</a>

<table class="table">
<thead>
<tr>
	<th>ID</th>
	<th>Tytuł</th>
	<th>Data utworzenia</th>
	<th>Opcje</th>
</tr>
</thead>

	<tbody>
		@foreach($posts as $post)
		<tr>
			<td>{{$post->id}}</td>
			<td>{{$post->title}}</td>
			<td>{{$post->Created}}</td>
			<td><a class="btn btn-warning" href="{{route('editpost',['id'=>$post->id])}}">Edytuj</a> 
			<form action="{{route('deletepost',['id'=>$post->id])}}" method="POST">
			    @method('DELETE')
			    @csrf
			     <button type="submit" class="btn btn-danger">Usuń</button>
			</form>
				</td>
		</tr>
		@endforeach

	</tbody>
	
</table>
{{ $posts->links() }}
@stop