@extends('layouts.backend')

@section('content')

<h2>Powiadomienia</h2>



<table class="table">
<thead>
<tr>

	<th>Treść</th>
	<th>Data</th>
	
</tr>
</thead>

	<tbody>
		@foreach($notifications as $notification)
		<tr>
			
			<td>{{$notification->content}}</td>
			<td>{{$notification->Created}}</td>
			
		</tr>
		@endforeach

	</tbody>
	
</table>
{{ $notifications->links() }}
@stop