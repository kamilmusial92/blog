@extends('layouts.backend')

@section('content')

<h2>Zarządzanie użytkownikami</h2>

<a  class="btn btn-success" href="{{route('createuser')}}">Stwórz użytkownika</a>

<table class="table">
<thead>
<tr>
	<th>ID</th>
	<th>Nazwa</th>
	<th>Prawa</th>

	<th>Opcje</th>
</tr>
</thead>

	<tbody>
		@foreach($users as $user)
		<tr>
			<td>{{$user->id}}</td>
			<td>{{$user->name}}</td>
			<td>{{$user->roles[0]->name}}</td>
			<td><a class="btn btn-warning" href="{{route('edituser',['id'=>$user->id])}}">Edytuj</a> 
			<form action="{{route('deleteuser',['id'=>$user->id])}}" method="POST">
			    @method('DELETE')
			    @csrf
			     <button type="submit" class="btn btn-danger">Usuń</button>
			</form>
				
		</tr>
		@endforeach
	</tbody>
</table>
{{ $users->links() }}
@stop