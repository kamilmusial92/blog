@extends('layouts.backend')

@section('content')

<h5 class="card-title">Nowy użytkownik</h5>


<form action="{{route('newuser')}}" method='POST'>
	@csrf
		<div class='row'>
			<div class='col-md-3'>
				<label>Imię</label>
			</div>

			<div class="form-group">
				
				<input type='text' name="name" value="{{old('name')}}">
			</div>
		</div>

		<div class='row'>
			<div class='col-md-3'>
				<label>E-mail</label>
			</div>

			<div class="form-group">
				
				<input type='email' name="email" value="{{old('emaile')}}">
			</div>
		</div>

		<div class='row'>
			<div class='col-md-3'>
				<label>Hasło</label>
			</div>

			 <div class="form-group">
				
				<input type='password' name="password" >
			</div>
		</div>

		<div class='row'>
			<div class='col-md-3'>
				<label>Rola</label>
			</div>

			 <div class="form-group">
			 	
				<select name="role" class="custom-select custom-select-sm">
				  
				  <option value="1">Użytkownik</option>
				  <option value="2">Administrator</option>
				  <option value="3">Redaktor</option>
				</select>
			</div>
		</div>
 <div class="card-footer">
      <button type="submit" class="btn btn-success">Stwórz</button>
    </div>

</form>



@stop