@extends('layouts.frontend')

@section('content')

<div class='container'>

@foreach($posts->chunk(4) as $chunked_post)
<div class='row'>
	@foreach($chunked_post as $post)
	<div class="card" style="width: 18rem;">
	  @if($post->file_path)
	  	<img src="/storage/images/{{$post->file_path}}" class="card-img-top" alt="...">
	  @endif
	  <div class="card-body">
	    <h5 class="card-title">{{$post->title}}</h5>
	    <p class="card-text">{{Str::limit($post->content,100)}}</p>
	    <a href="{{route('post',['id'=>$post->id])}}" class="btn btn-primary">Czytaj</a>
	  </div>
	</div>
	@endforeach
</div>

@endforeach

		{{ $posts->links() }}
</div>

@stop