@extends('layouts.frontend')

@section('content')

<div class='container'>


	<div class='row'>

		<div class="card" >
		  @if($post->file_path)
		  	<img height="20%" src="/storage/images/{{$post->file_path}}" class="card-img-top" alt="...">
		  @endif
		  <div class="card-body">
		    <h5 class="card-title">{{$post->title}}</h5>
		    <p class="card-text">{{$post->content}}</p>
		    <a href="{{URL::previous()}}" class="btn btn-primary">Wstecz</a>
		  </div>
		</div>

	</div>
</div>



		


@stop