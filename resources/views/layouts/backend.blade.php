<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Panel zarządzania</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    
    </head>
    <body>
        <div class='container'>
            <div class="row">
                <div class='col-md-3'>
                    <ul class="list-group">
                        <li class="list-group-item"><a href="{{route('homepage')}}">Strona główna</a></li>

                        <li class="list-group-item"><a href="{{route('admin_notification')}}">Powiadomienia</a></li>

                         <li class="list-group-item"><a href="{{route('adminHome')}}">Zarządzaj postami</a></li>

                        @if(Auth::user()->hasRole(['Administrator']))
                        <li class="list-group-item"><a href="{{route('admin_users')}}">Zarządzaj użytkownikami</a></li>
                        @endif
                       
                        <li class="list-group-item">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Wyloguj</a> 
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                               @csrf
                           </form>
                        </li class="list-group-item">
                    </ul>
                </div>
                <div class='col-md-7'>
                    <div class="card">
                         <div class="card-body">
                        @include('notification')

                        @include('form_errors')

                        @yield('content')
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </body>
</html>
