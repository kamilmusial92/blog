<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Blog</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    
    </head>
    <body>
        <ul>
            @guest
            <li><a href="{{route('login')}}">logowanie</a></li>
           
            <li><a href="{{route('register')}}">rejestracja</a></li>
            @endguest

            @auth
                @if(Auth::user()->hasRole(['Administrator','Redaktor']))
                    <li>
                        <a href="{{route('adminHome')}}">Panel admina</a>
                    </li>
                @endif
             <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Wyloguj</a> 
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                   @csrf
               </form>
            </li>
            @endauth
        </ul>

        @include('notification')

        @include('form_errors')

        @yield('content')

    </body>
</html>
