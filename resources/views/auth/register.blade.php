@extends('layouts.frontend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Rejestracja</div>

                <div class="card-body">
                      @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                           <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Imię</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="name" value="{{ old('name')}}" required >

                             
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Adres e-mail</label>

                            <div class="col-md-6">
                                <input  type="email" class="form-control" name="email" value="{{ old('email')}}" required >

                             
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Hasło</label>

                            <div class="col-md-6">
                                <input  type="password" class="form-control" name="password" required >

                                
                            </div>
                        </div>

                           <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Powtórz hasło</label>

                            <div class="col-md-6">
                                <input  type="password" class="form-control" name="password confirmation" required >

                                
                            </div>
                        </div>

                    

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Zarejestruj się
                                </button>

                               
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
