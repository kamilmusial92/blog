<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','FrontendController@index')->name('homepage');
Route::get('/post/{id}','FrontendController@post')->name('post');

Route::group(['prefix'=>'admin','middleware'=>'CheckUser'], function(){
	Route::get('/','BackendController@index')->name('adminHome');

	Route::get('/notifications','BackendController@notifications')->name('admin_notification');

	Route::get('/users','BackendController@users')->name('admin_users');
	Route::get('/users/create','BackendController@createuser')->name('createuser');
	Route::delete('/users/edit/{id}','BackendController@deleteuser')->name('deleteuser');
	Route::get('/users/edit/{id}','BackendController@edituser')->name('edituser');
	Route::post('/users/saveuser/{id}','BackendController@saveuser')->name('saveuser');
	Route::get('/users/resetpassword/{id}','BackendController@resetpassword')->name('resetpassword');
	Route::post('/users/newuser','BackendController@newuser')->name('newuser');


	Route::get('/post/create','BackendController@createpost')->name('createpost');
	Route::get('/post/edit/{id}','BackendController@editpost')->name('editpost');
	Route::delete('/post/edit/{id}','BackendController@deletepost')->name('deletepost');
	Route::post('/post/savepost/{id}','BackendController@savepost')->name('savepost');
	Route::post('/post/newpost','BackendController@newpost')->name('newpost');
	
});

Auth::routes();



//Route::get('/login','LoginController@login')->name('login');
//Route::get('/register','RegisterController@register')->name('register');