Dane do logowania: 
login: test@admin.pl
hasło: haslo123

#
Etapy powstawania projektu:
#
	1. Moduł logowania i rejestracji
		-stworzenie przykładowego użytkownika poprzez seeder. 
		-stworzenie widoku do zalogowania, resetowanie hasła 
		-stworzenie widoku do rejestracji, wysyłanie potwierdzenie mailem poprzez Event.
		-stworzenie tabeli "role", wraz z seederem
		-stworzenie middleware CheckUser który będzie sprawdzał czy logujący się użytkownik ma role Administrator bądź Redaktor. Użytkownik z rolą Użytkownik będzie przekierowywany do strony głównej.
		

	
	2. Moduł zarządzania użytkownikami
		-stworzenie widoku panelu admina
		-panel zarządzania użytkownikami - dodawanie/edycja użytkowników/nadawanie praw
		
	3. Moduł dodawania i  edycji postów
		-stworzenie widoku do dodawania/edycji postów
		-upload obrazków
		
	4. Wyświetlanie posów na stronie głównej
	 -listowanie postów i paginacja
	 -cachowanie listy postów
	
	5.Powiadomienia
	-powiadomienia zrobione metodą Eventów.
	
	6.Konfiguracja aplikacji pod Redis.
	
	###Instalacja###
	Po pobraniu plików z repozytorium należy pobrać pozostałe paczki komendą: composer install. Następnie gdy chcemy obsługiwać aplikację na innej bazie danych należy zmienić dane w pliku .env a następnie wgrać tabele do bazy komedną: php artisan migrate --seed
	
	###Mailing###
	Dla sprawdzenia poprawności wysyłania emaili należy założyć konto na mailtrap.io i zmienić dane konfiguracyjne w pliku .env
	
	
	###Baza danych###
	Domyślnie jest to postawione na darmowej bazie danych Mysql 8.0. Aplikacja przez to przymula, więc można sobie podpiąć pod to inną bazę danych, plik konfiguracyjny .env
	
	###Cachowana wersja###
	W pliku .env należy zamienić wartość APP_ENV na production.
	
	###Redis###
	Aktualna wersja jest skonfigurowana pod Redis. Gdyby zaszła potrzeba wyłączenia tego wystarczy w pliku .env zmienić wartośći CACHE_DRIVER i SESSION_DRIVER na file
	
	
	